import {useState, useEffect} from 'react';

// import productsData from '../data/productsData';
import ProductsCard from '../components/ProductsCard';

export default function Products() {

  // to store the products retrieved from the database
  const [products, setProducts] = useState([]);

  // console.log(productsData);

  

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setProducts(data.map(product => {
        return(
          <ProductsCard key={product._id} product={product} />
        )
      }))
    })
  }, [])

  return (
    <>
    {products}
    </>
  )
}
