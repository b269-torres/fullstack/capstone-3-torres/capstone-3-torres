import {useState, useEffect, useContext} from 'react';

import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function Register() {


// export default function Error() {

//     const data = {

//         title: "Error 404 - Page not found.",
//         content: "The page you are looking for cannot be found.",
//         destination: "/",
//         label: "Back to Home"

//     }


    // to store and manage value of the input fields

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [password2, setPassword2] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const {user, setUser} = useContext(UserContext);
   // const navigate = useNavigate();

   

    // function to simulate user registration
  /*  function registerUser(e) {
        e.preventDefault();

        // Clear input fields
        setFirstName("");
        setLastName("");
        setContactNumber("");
        setEmail("");
        setPassword1("");
        setPassword2("");

        //alert("Thank you for registering!");
    };*/

  /*  function registerNewUser(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,
                password2: password2

            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            // If no user information is found, the "access" property will not be available and will return undefined
            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
            if(typeof data.access !== "undefined") {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                  Swal.fire({
                    title: "Duplication email found",
                    icon: "error",
                    text: "Please Provide a different email"
                })
            }
    });

        setEmail("");
        setPassword("");
        setPassword2("");

}*/

    function registerNewUser(e)
        {
            e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/users/register`,
            {
                method: "POST",
                headers:
                {
                    "Content-Type" : "application/json"
                },
                body: JSON.stringify(
                {
                    
                    email: email,
                    password: password,
                    password2: password2
                })
            }).then(res =>res.json())
            .then(data => {
                    if (data.error)
                    {
                        Swal.fire({
                            title: `${data.error}`,
                        icon: "error",
                      
                        })
                    }
                    if (data.success)
                    {
                        Swal.fire({
                        title: `${data.success}`,
                        icon: "success",
                        text: "Please proceed to login."
                        })

                        //navigate("/login");
                    }
                })

            
            setEmail("");
            setPassword("");
            setPassword2("");
        }


/*    const retrieveUserDetails = (token) => {
            // The token will be sent as part of the request's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Global user state for validation accross the whole app
                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        };
*/

        useEffect(() => {
            if( email !== "" && password !== "" && password2 !=="" && password === password2) {
                setIsActive(true);
            } else {
                setIsActive(false);
            };
        }, [email, password, password2]);
    return (
        (user.id!== null) ?
        <Navigate to="/login" />
        :
        <Form onSubmit={(e) => registerNewUser(e)} >
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
            }
            
        </Form>
    )

}
