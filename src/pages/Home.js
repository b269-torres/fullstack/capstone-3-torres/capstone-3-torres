import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductUser from "../components/ProductUser"

export default function Home() {

	const data = {
		title: "Wellcome to Fourth Wall",
		content: "A game Company that sell unique NFT Item ",
		destination: "/register",
		label: "Buy Now"
	}
	return (
		<>
		< Banner data={data}/>
      	< Highlights/>
      	< ProductUser/>
		</>
	)
}
