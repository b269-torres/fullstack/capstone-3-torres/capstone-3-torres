import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'
import {Button} from 'react-bootstrap'
//import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductsCard';
//import CreateProducts from '../components/CreateProducts';

export default function Products() {

  // to store the course retrieved from the database 
  const [products, setProducts]= useState([]);

  // console.log(coursesData);

  // const courses = coursesData.map(course => {
  //  return (
  //    < CourseCard key={course.id} course = {course} />
  //  )
  // })

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(data.map(products => {
          return(

            <ProductCard key={products._id} products={products} />
          )
        }))
      })
    }, [])


  return (
    <>
    <br/>
    <Button className="bg-primary" as={Link} to={`/create`}>Create new Product</Button>
    {products}
    </>
  )
}
