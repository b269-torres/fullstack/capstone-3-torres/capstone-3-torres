import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import React from 'react'
import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import './App.css';


import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import AdminPage from './components/AdminPage';
import CreateProducts from './components/CreateProducts';
import ProductUser from './components/ProductUser';
import ProductCheckout from './components/ProductCheckout';





import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import ProductCatalog from './pages/ProductCatalog';
import UserOrder from './pages/UserOrder';


function App() {

     const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

// Used to check if user information is properly stored upon login and the localStorage information 
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // User is logged in
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);
  return (
     <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
         < AppNavbar/>
          <Container>
          <Routes>
        < Route path="/" element={<Home/>}/>
    < Route path="/login" element={<Login />}/>
    < Route path="/register" element={<Register />}/>
    < Route path="/logout" element={<Logout/>}/>
    < Route path="/productCatalog" element={<ProductCatalog/>}/>
    < Route path="/products/:productId" element={<ProductView/>}/>
    < Route path="/:productId/update" element={<AdminPage/>}/>
    < Route path="/create" element={<CreateProducts/>}/>
    < Route path="/allProduct" element={<ProductView/>}/>


    < Route path="/allActiveProduct" element={<ProductUser/>}/>
    < Route path="/:productId/Checkout" element={<ProductCheckout/>}/>
   
  
          </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  )
}

export default App;
