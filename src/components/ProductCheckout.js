// import { useState, useEffect, useContext } from 'react';
// import UserContext from '../UserContext';
// import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
// import Swal from 'sweetalert2';
// import { useParams, useNavigate, Link } from 'react-router-dom';

// export default function ProductView() {

//   const { user } = useContext(UserContext);
//   const navigate = useNavigate();
//   const { productId } = useParams();
// const [name, setName] = useState("");
//   const [description, setDescription] = useState("");
//   const [price, setPrice] = useState(0);
//   const [product, setProduct] = useState({});

//  const order = (productId) => {
//   fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
//     method: "POST",
//     headers: {
//       'Content-Type': 'application/json',
//       Authorization: `Bearer ${localStorage.getItem('token')}`,
//     },
//     body: JSON.stringify({
//       productId: productId
//     })
//   })
//     .then(res => res.json())
//     .then(data => {
//       console.log(data); // check that data is being populated
//       if (data === true) {
//         console.log("Enrolled successfully"); // check that this message is being logged
//         Swal.fire({
//           title: "Successfully enrolled",
//           icon: "success",
//           text: "You have successfully enrolled for this course."
//         });
//         navigate("/allActiveProduct");
//       } else {
//         console.log("Something went wrong"); // check that this message is being logged
//         Swal.fire({
//           title: "Something went wrong",
//           icon: "error",
//           text: "Please try again."
//         });
//       }
//     })
//     .catch(error => {
//       console.log("Error occurred during the fetch request:", error);
//     })
// }


//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
//       .then(res => res.json())
//       .then(data => {
//         console.log(data);
//         setProduct(data);

//         setName(data.name);
//         setDescription(data.description);
//         setPrice(data.price);
//       })
//   }, [productId])

//   return (
//     <Container>
//       <Row className="mt-3 mb-3">
//         <Col lg={{ span: 6, offset: 3 }}>
//           <Card>
//             <Card.Body className="text-center">
//               <Card.Title>{name}</Card.Title>
//               <Card.Title>{description}</Card.Title>
//               <Card.Title>{price}</Card.Title>
//               <Card.Subtitle>Order this Product?</Card.Subtitle>
//               {
//                 (user.id !== null) ?
//                 <Button variant="primary" onClick={() => order(productId)}>Order</Button>
//                 :
//                 <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
//               }
//             </Card.Body>
//           </Card>
//         </Col>
//       </Row>
//     </Container>
//   )
// }


import { useState, useContext, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { Card, Button, Container } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function ProductCheckout() {
  const navigate = useNavigate();
  const [product, setProduct] = useState({});
  const [quantity, setQuantity] = useState(0);
  const [total, setTotal] = useState(0);
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    const fetchCartData = async () => {
      try {
        // Fetch the user's details
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        });
        const userData = await response.json();

        // Fetch the cart data
        const cartResponse = await fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        });
        const cartData = await cartResponse.json();

        // Set the state with the fetched data
        setProduct(cartData.product);
        setQuantity(cartData.quantity);
        setTotal(cartData.quantity * cartData.product.price);
        setUser(userData);
      } catch (error) {
        console.error(error);
      }
    };
    fetchCartData();
  }, []);

  const handleCheckOut = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token"),
        },
        body: JSON.stringify({
          product: product._id,
          quantity: quantity,
          total: total,
        }),
      });
      const data = await response.json();
      if (response.ok) {
        Swal.fire({
          title: "Success!",
          text: data.message,
          icon: "success",
          confirmButtonText: "OK",
        }).then(() => {
          navigate("/");
        });
      } else {
        Swal.fire({
          title: "Error!",
          text: data.message,
          icon: "error",
          confirmButtonText: "OK",
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Container>
      <Card>
        <Card.Body className="text-center">
 {/*         <Card.Title>{product.name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{product.description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {product.price}</Card.Text>*/}
          <Card.Subtitle>Quantity:</Card.Subtitle>
          <input
            type="number"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          />
{/*          <Card.Subtitle>Total Amount:</Card.Subtitle>
          <Card.Text>PhP {total}</Card.Text>*/}
          <Button className="btn btn-danger" onClick={handleCheckOut}>
            Purchase
          </Button>
          <Button className="btn btn-danger" as={Link} to="/products">
            Back to products
          </Button>
        </Card.Body>
      </Card>
    </Container>
  );
}