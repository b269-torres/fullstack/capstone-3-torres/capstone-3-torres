import {Link} from 'react-router-dom'
//import { useParams} from "react-router";
//import {useState} from 'react';
//import {useEffect} from 'react';

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function ProductsCard({products}) {
//const params = useParams();
//const {productId} = useParams();
//const [isActive] = useState(products.isActive);

    // Deconstruct the course properties into their own variables
    const { name, description, price, _id ,quantity} = products;
   const isActive = Boolean(products.isActive);






return (



    <Row className="mt-3 mb-3 justify-content-md-center">

            <Col xs={12}>
            
            <br/>
            <br/>
                <Card className="cardHighlight p-0 ">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <br/>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Quantity</Card.Subtitle>
                        <Card.Text>{quantity}</Card.Text>
                        <Card.Subtitle>Availability</Card.Subtitle>
                        <Card.Text>{isActive ? "Available" : "Not Available"}</Card.Text>
                      {/*  <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>
                    </Card.Body>*/}

                        <Button className="bg-primary" as={Link} to={`/${_id}/update`}>Update</Button>
                  
                   
                    </Card.Body>

                </Card>
            </Col>
    </Row>        
    )

}