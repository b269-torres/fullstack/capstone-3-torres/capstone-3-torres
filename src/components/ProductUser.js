import { useEffect, useState } from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import {Link} from 'react-router-dom'

function ActiveProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active
      `)
      .then((response) => response.json())
      .then((data) => setProducts(data))
      .catch((error) => console.error(error));
  }, []);

  return (

        <Row className="mt-3 mb-3 justify-content-md-center">
         
                <Col xs={12}>
                 
                <br/>
                <br/>
               {products.map((product, index) => (
                    <Card key={index} className="cardHighlight p-0 ">
                        <Card.Body>
                            <Card.Title><h4>{product.name}</h4></Card.Title>
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{product.description}</Card.Text>
                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{product.price}</Card.Text>
                            <Card.Subtitle>Quantity</Card.Subtitle>
                            <Card.Text>{product.quantity}</Card.Text>
                          
                          {/*  <Card.Subtitle>Count: {count}</Card.Subtitle>
                            <Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>
                        </Card.Body>*/}


                           <Button className="bg-primary" as={Link} to={`/:productId/Checkout`}>Order Product</Button>
                      
                      
                        </Card.Body>

                    </Card>
                    ))}
                </Col>
                 
        </Row>     

        )

    }

export default ActiveProducts;


