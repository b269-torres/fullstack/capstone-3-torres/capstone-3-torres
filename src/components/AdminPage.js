

import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router";
import Swal from 'sweetalert2';

export default function Edit() {
  const [form, setForm] = useState({
    name: "",
    description: "",
    price: "",
    quantity: "",
    products: [],
  });
  const params = useParams();
  const navigate = useNavigate();
  const { productId } = useParams();
  
  useEffect(() => {
    async function fetchData() {
      const id = productId.toString();
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${id}`);
 
      if (!response.ok) {
        const message = `An error has occurred: ${response.statusText}`;
        window.alert(message);
        return;
      }
 
      const record = await response.json();
      if (!record) {
        window.alert(`Record with id ${id} not found`);
        navigate("/");
        return;
      }
 
      setForm(record);
    }
 
    fetchData();
 
    return;
  }, [productId, navigate]);
 
  function updateForm(value) {
    return setForm((prev) => {
      return { ...prev, ...value };
    });
  }
 
  async function onSubmit(e) {
    e.preventDefault();
    const editedPerson = {
      name: form.name,
      description: form.description,
      price: form.price,
      quantity: form.quantity,
    };
 
    await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: "PUT",
      body: JSON.stringify(editedPerson),
      headers: {
        'Content-Type': 'application/json'
      },
    });
    
    e.target.reset();
    navigate("/productCatalog");
  }
 
  function deactivateProduct(e) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      } 
    }).then(res =>res.json()).then(data => {
      if (data.error) {
        Swal.fire({
          title: `Error`,
          icon: "error",
          // text: "Please provide a different email."
        })
      } else {
        Swal.fire({
          title: `Deactivate Successful`,
          icon: "success",
          // text: "Please proceed to login."
        })
      }
    })
  }

  function activateProduct(e) {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
      method: "PATCH",
      headers: {
        "Content-Type" : "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      } 
    }).then(res =>res.json()).then(data => {
      if (data.error) {
        Swal.fire({
          title: `Error`,
          icon: "error",
          // text: "Please provide a different email."
            })
          }
          else
          {
            Swal.fire({
                      title: `Activate Successful`,
                      icon: "success",
                      // text: "Please proceed to login."
                    })
            
           // navigate(`/productCatalog`);
          }
        })
  }

 // This following section will display the form that takes input from the user to update the data.
 return (

   <div>
     <h3>Update Product</h3>
     <form onSubmit={onSubmit}>
       <div className="form-group">
         <label htmlFor="name">Name: </label>
         <input
           type="text"
           className="form-control"
           id="name"
           value={form.name}
           onChange={(e) => updateForm({ name: e.target.value })}
         />
       </div>
       <div className="form-group">
         <label htmlFor="description">Description: </label>
         <input
           type="textarea"
           className="form-control"
           id="description"
           value={form.description}
           onChange={(e) => updateForm({ description: e.target.value })}
         />
       </div>
         <div className="form-group">
         <label htmlFor="price">Price: </label>
         <input
           type="number"
           className="form-control"
           id="price"
           value={form.price}
           onChange={(e) => updateForm({ price: e.target.value })}
         />
       </div>
    <div className="form-group">
  <label htmlFor="quantity">Quantity: </label>
  <input
    type="number"
    className="form-control"
    id="quantity"
    value={form.quantity}
    onChange={(e) => updateForm({ quantity: e.target.value })}
  />
</div>
       <br />
 
       <div className="form-group">
         <input

           type="submit"
           value="Update Product"
           className="btn btn-primary"
         />
       </div>
       <br/>
       <div className="form-group" onClick={() => deactivateProduct(`${productId}`)}>
      
         <input
           type="submit"
           value="Deactivate Product"
           className="btn btn-warning"
         />
       </div>
         <div className="form-group" onClick={() => activateProduct(`${productId}`)}>
      
         <input
           type="submit"
           value="Activate Product"
           className="btn btn-primary"
         />
       </div>
     </form>
   </div>
 );
}