import { useState, useEffect } from 'react';
import {  Row, Col, Card } from 'react-bootstrap';


export default function ProductList() {

  const [products, setProducts] = useState([]);
  const [isActive] = useState(products.isActive);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  



  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allProducts
      `)
      .then(response => response.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  return (
     <Row className="mt-3 mb-3 justify-content-md-center">

            <Col xs={12}>
            
            <br/>
            <br/>
                <Card className="cardHighlight p-0 ">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Quantity</Card.Subtitle>
                        <Card.Text>{quantity}</Card.Text>
                        <Card.Subtitle>Active</Card.Subtitle>
                        <Card.Text>{isActive}</Card.Text>
                      {/*  <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>
                    </Card.Body>*/}

                   
            
                    </Card.Body>

                </Card>
            </Col>
    </Row>        
    )

}